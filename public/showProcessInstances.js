async function showProcessInstances(event){
  removeTable();
  //removeDiagram();
  removeActiveBtn('showInstances');
  event.target.classList.add('active');
  
  let responseXML = await fetch('/process-definition/' + event.target.id + '/xml');
  let someXML = await responseXML.json();
  var modelXML = someXML.bpmn20Xml; 
  
  let instanceList = document.createElement('ul');
  instanceList.classList.add('list-group');

  let header = document.createElement('li');
  header.classList.add('list-group-item', 'bg-light');
  header.innerHTML = 'Instances : ';
  instanceList.appendChild(header);

  let response = await fetch('/instances/' + event.target.id);
  let data = await response.json();

  for(instance of data){
    let item = document.createElement('li');
    item.classList.add('list-group-item');
    if(instance.state === 'ACTIVE'){
      item.classList.add('bg-primary');
    } else if(instance.state === 'COMPLETED'){
      item.classList.add('bg-secondary');
    } else if(instance.state.includes('TERMINATED')){
      item.classList.add('bg-danger');
    }

    let itemDiv = document.createElement('div');
    
    let itemName = document.createElement('p');
    itemName.innerHTML = instance.id;
    itemDiv.appendChild(itemName);

    let diagramBtn = document.createElement('button');
    diagramBtn.classList.add('btn', 'btn-success', 'cursor-pointer', 'border', 'border-dark', 'showDiagram');
    diagramBtn.innerHTML = "Show diagram";
    diagramBtn.style.marginRight = '5px'; 
    diagramBtn.style.marginBottom = '5px'; 
    //diagramBtn.style.borderColor = '#4CAF50';
    diagramBtn.modelXML = modelXML;
    diagramBtn.id = instance.id;
    diagramBtn.addEventListener('click', showDiagram);
    itemDiv.appendChild(diagramBtn);

    let response = await fetch('/variables/' + instance.id);
    let data = await response.json();

    if(Object.keys(data).length !== 0){
      let showVariables = document.createElement('button');
      showVariables.classList.add('btn', 'btn-success', 'cursor-pointer', 'border', 'border-dark', 'showVariables');
      showVariables.innerHTML = "Show Variables";
      showVariables.style.marginBottom = '5px';
      showVariables.id = instance.id;
      showVariables.addEventListener('click', showProcessVariables);
      itemDiv.appendChild(showVariables);
    } else {
      let noVariables = document.createElement('button');
      noVariables.classList.add('btn','btn-warning', 'disabled');
      noVariables.innerHTML = "No variables";
      noVariables.style.marginBottom = '5px';
      itemDiv.appendChild(noVariables);
    }

    let deleteInstanceBtn = document.createElement('button');
    deleteInstanceBtn.classList.add('cursor-pointer', 'btn', 'btn-danger', 'border', 'border-dark');
    deleteInstanceBtn.innerHTML = "Delete instance";
    
    deleteInstanceBtn.state = instance.state;
    deleteInstanceBtn.id = instance.id;

    deleteInstanceBtn.addEventListener('click', deleteInstance)
    itemDiv.appendChild(deleteInstanceBtn);

    item.appendChild(itemDiv);
    instanceList.appendChild(item);
  }
  removeOldDisplay(middleDiv);
  middleDiv.appendChild(instanceList);
}

async function deleteInstance(){
  if(event.target.state === "ACTIVE"){
    await fetch('/process-instance-delete/' + event.target.id)
    .catch((error) => {
      console.log('Error:', error);
    });
  } else {
    await fetch('/process-instance-history-delete/' + event.target.id)
    .catch((error) => {
      console.log('Error:', error);
    });
  }
}

async function showDiagram(){
  let modelXML = event.target.modelXML;

  //removeTable();
  removeActiveBtn('showDiagram');
  event.target.classList.add('active');
  
  let response = await fetch('/activity-instance/' + event.target.id);
  let data = await response.json();

  //let finishedTasks = [];
  //data.forEach(json => finishedTasks.push(json.activityId));

  let canvasDiv = document.getElementById('canvas');
  canvasDiv.innerHTML = "";
  var viewer = new BpmnJS({ container: '#canvas' });
  var canvas = viewer.get('canvas');
  
  let badLink = document.getElementsByClassName('bjs-container')[0].firstChild;
  document.getElementsByClassName('bjs-container')[0].removeChild(badLink);

  await viewer.importXML(modelXML, function(err) {
    if (err) {
        console.log(err);

    } else {
      var overlays = viewer.get('overlays');
      var elementRegistry = viewer.get('elementRegistry');
      //console.log(elementRegistry);
      
      data.forEach(element => {
        var shape = elementRegistry.get(element.activityId);
        var current = (element.endTime === null);
        //console.log(shape);
        //console.log(current);
        
        if(!current){
          var $overlayHtml = $('<div class="highlight-overlay-green">')
          .css({
            width: shape.width,
            height: shape.height
          });
        } else {
          var $overlayHtml = $('<div class="highlight-overlay-blue">')
          .css({
            width: shape.width,
            height: shape.height
          });
        } 

        overlays.add(shape, {
          position: {
            top: 0,
            left: 0
          },
          html: $overlayHtml
        });
      });
     
      canvas.zoom('fit-viewport');
    }
  });
}