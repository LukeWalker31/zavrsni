let leftDiv = document.getElementById('left');
let middleDiv = document.getElementById('middle');
let rightDiv = document.getElementById('right');
let rightUpDiv = document.getElementById('canvas');
let rightDownDiv = document.getElementById('right-down');

async function start(){
  let response = await fetch('/process-definitions');
  let data = await response.json();

  let definitionList = document.createElement('ul');    
  definitionList.id = 'definitionList'; 
  definitionList.classList.add('list-group');


  let header = document.createElement('li');
  header.classList.add('list-group-item', 'bg-light');
  header.innerHTML = 'Definitions : ';
  definitionList.appendChild(header);

  for(definition of data){                          
    let item = document.createElement('li');
    item.classList.add('list-group-item', 'bg-secondary')

    let itemDiv = document.createElement('div');

    let itemName = document.createElement('p');
    itemName.innerHTML = definition.name;
    itemDiv.appendChild(itemName);

    let canvasDiv = document.createElement('div');
    canvasDiv.id = definition.id;
    //containerString = '#canvas' + definition.id;
    console.log(canvasDiv.id);
    //console.log(containerString);
    //canvasDiv.innerHTML = "";
    itemDiv.appendChild(canvasDiv);

    let moreData = document.createElement('button');
    moreData.classList.add('cursor-pointer', 'btn', 'btn-info', 'border', 'border-dark');
    moreData.innerHTML = "Details";
    moreData.id = definition.id;
    moreData.addEventListener('click', showDetails)
    itemDiv.appendChild(moreData);

    let statisticsResponse = await fetch('/process-definition-statistics/' + definition.id);  
    let statisticsData = await statisticsResponse.json();

    let noInstances = (statisticsData.length === 0);

    if(!noInstances) {
      let showInstances = document.createElement('button');
      showInstances.classList.add('cursor-pointer', 'btn', 'btn-success', 'border', 'border-dark', 'showInstances');
      showInstances.style.marginLeft = '5px';
      showInstances.innerHTML = "Show instances";
      showInstances.id = definition.id;
      showInstances.addEventListener('click', showProcessInstances);
      itemDiv.appendChild(showInstances);
    } else {
      let noInstances = document.createElement('button');
      noInstances.classList.add('btn','btn-warning', 'disabled');
      noInstances.style.marginLeft = '5px';
      noInstances.innerHTML = "No instances";
      itemDiv.appendChild(noInstances);
    }

    let deleteDefinitionBtn = document.createElement('button');
    deleteDefinitionBtn.classList.add('cursor-pointer', 'btn', 'btn-danger', 'border', 'border-dark');
    deleteDefinitionBtn.style.marginTop = '5px';
    deleteDefinitionBtn.innerHTML = "Delete definition";
    deleteDefinitionBtn.id = definition.id;
    deleteDefinitionBtn.addEventListener('click', deleteDefinition)
    itemDiv.appendChild(deleteDefinitionBtn);

    let newInstanceBtn = document.createElement('button');
    newInstanceBtn.classList.add('cursor-pointer', 'btn', 'btn-success', 'border', 'border-dark');
    newInstanceBtn.style.marginTop = '5px';
    newInstanceBtn.innerHTML = "Create new Instance";
    newInstanceBtn.id = definition.id;
    newInstanceBtn.addEventListener('click', newInstance);
    itemDiv.appendChild(newInstanceBtn);

    item.appendChild(itemDiv);
    definitionList.appendChild(item);
  }
  leftDiv.appendChild(definitionList);

  //console.log(definitionList.getElementsByTagName('li'));
  for(item of definitionList.getElementsByTagName('li')){
    if(item === definitionList.firstChild){
      continue;
    }
    console.log(item);
    let canvasDiv = item.firstChild.getElementsByTagName('div')[0];
    console.log(canvasDiv);
    var viewer = new BpmnJS({ container: '#' + canvasDiv.id});
    console.log('got here');
  }
}

async function showDetails(){
  let response = await fetch('/process-definitions/' + event.target.id);
  let data = await response.json();

  let outputString = "Process Definiton Id: " + data.id + "\n";
  outputString += "Version: " + data.version + "\n";
  outputString += "Description: " + data.description + "\n";
  outputString += "Diagram: " + data.diagram + "\n";
  outputString += "Resource: " + data.resource + "\n";
  outputString += "Deployment Id: " + data.deploymentId + "\n";

  alert(outputString);
}

async function deleteDefinition(){
  console.log('delete1');
  await fetch('/process-definition-delete/' + event.target.id)
  .catch((error) => {
    console.log('Error:', error);
  });
  console.log('delete4');
  
 /*
  while(leftDiv.firstChild){
    leftDiv.removeChild(leftDiv.firstChild);
  }
  console.log(leftDiv.children);
  rightDownDiv.children = [];
  start();
  */
 //window.location.reload();
 console.log('we reloaded');
}

async function newInstance(){
  await fetch('/start-instance/' + event.target.id, {method: "POST"})
  .catch((error) => {
    console.log('Error:', error);
  });
}
/*
function removeDiagram(){
  if(rightUpDiv.firstChild){
   rightUpDiv.removeChild(rightUpDiv.firstChild);
  }
}
*/

function removeTable(){
  let tableToMaybeRemove = document.getElementById('variableTable');
  if(tableToMaybeRemove){
   rightDownDiv.removeChild(tableToMaybeRemove);
  }
}

function removeActiveBtn(btn){
  for(listItem of document.getElementsByClassName(btn)){
    listItem.classList.remove('active');
  }
}

function removeOldDisplay(mainDiv){
  while(mainDiv.firstChild) {
    mainDiv.removeChild(mainDiv.firstChild);
  }
}

start();