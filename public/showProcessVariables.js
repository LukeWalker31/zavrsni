async function showProcessVariables() {
  removeActiveBtn('showVariables');
  event.target.classList.add('active');

  let response = await fetch('/variables/' + event.target.id)
  let data = await response.json();

  let rows = [];
  for(key of Object.keys(data)){
    entries = Object.entries(data[key]);
    entries = entries.filter(a => a[1] !== null); //samo pokazi podatke koje postoje

    rows.push([['number', key], ...entries]);
  }

  let table = document.createElement('table');
  table.classList.add('table', 'text-light');
  table.id = 'variableTable';

  let thead = document.createElement('thead');
  let tbody = document.createElement('tbody');
  let tr = document.createElement('tr');
  
  for(column of rows[0]){
    let th = document.createElement('th');
    th.innerHTML = column[0];
    tr.appendChild(th);
  }
  for(row of rows){
    let tr = document.createElement('tr');
    for(column of row) {
      let td = document.createElement('td');
      td.innerHTML = column[1];
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
  }
  thead.appendChild(tr);
  table.appendChild(thead);
  table.appendChild(tbody)

  removeOldDisplay(rightDownDiv);
  rightDownDiv.appendChild(table);
}