const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const app = express();
app.use(bodyParser.json());
app.use(express.static('public'));
const PORT = process.env.PORT || 4001;

app.get('/process-definitions', (req, res, next) => { //za prikaz liste procesa
  fetch('http://localhost:8082/engine-rest/process-definition')
  .then(response => {
    return response.json();
  })
  .then(data => {
    res.status(200).json(data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/process-definitions/:id', (req, res, next) => { //za prikaz detalja pojedinog procesa
  fetch('http://localhost:8082/engine-rest/process-definition/' + req.params.id)
  .then(response => {
    return response.json();
  })
  .then(data => {
    res.status(200).json(data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/process-definition-delete/:id', (req, res, next) => { //za brisanje procesa (i sve njegove instance)
  fetch('http://localhost:8082/engine-rest/process-definition/' + req.params.id +'?cascade=true', {method: 'delete'})
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/process-definition-statistics/:id', (req, res, next) => { //za provjeru ima li proces aktivnih instanci (PROMIJENITI)
  fetch('http://localhost:8082/engine-rest/process-definition/' + req.params.id + '/statistics')
  .then(response => {
    return response.json();
  })
  .then(data => {
    res.status(200).json(data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/process-definition/:id/xml', (req, res, next) => { //za dohvacenje XML procesa za prikaz
  fetch('http://localhost:8082/engine-rest/process-definition/' + req.params.id + '/xml')
  .then(response => {
    return response.json();
  })
  .then(data => {
    res.status(200).send(data);
  })
});

app.get('/instances/:id', (req, res, next) => { //za stvaranje listi instanci pojedinog procesa
  fetch('http://localhost:8082/engine-rest/history/process-instance?processDefinitionId=' + req.params.id)
                
  .then(response => {
    return response.json();
  })
  .then(data => {
    res.status(200).json(data);
  });
});

app.get('/process-instance-history-delete/:id', (req, res, next) => { //za brisanje dovrsene instance
  fetch('http://localhost:8082/engine-rest/history/process-instance/' + req.params.id, {method: 'delete'})
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/process-instance-delete/:id', (req, res, next) => { //za brisanje aktivne instance
  fetch('http://localhost:8082/engine-rest/process-instance/' + req.params.id, {method: 'delete'})
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.post('/start-instance/:id', (req, res, next) => {// za pokrenuti novu instancu nekog procesa
  fetch('http://localhost:8082/engine-rest/process-definition/' + req.params.id +'/start', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({})
  })
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/activity-instance/:id', (req, res, next) => {//za dohvacenje putanje Token-a instance
  fetch('http://localhost:8082/engine-rest/history/activity-instance?processInstanceId=' + req.params.id)
                
  .then(response => {
    return response.json();
  })
  .then(data => {
    res.status(200).json(data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.get('/variables/:id', (req, res, next) => { //za dohvat varijabli instance
  fetch('http://localhost:8082/engine-rest/history/variable-instance?processInstanceIdIn=' + req.params.id)
  .then(response => {
    return response.json();
  })
  .then(data => {
    //console.log(data);
    res.status(200).json(data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
});

app.listen(PORT);
console.log('Server started! At http://localhost:' + PORT);